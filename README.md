# Cache S3

File cache backend using S3 protocol (e.g. Minio, AWS S3).

## Installation

Plugin must be enabled systemwide via `TTRSS_PLUGINS`.

## Configuration

The following options are required:

```ini
TTRSS_CACHE_S3_ENDPOINT=http://example.com:9000
TTRSS_CACHE_S3_BUCKET=bucket-name
TTRSS_CACHE_S3_REGION=us-east-1
```

The following options are required if you're directly providing access keys (i.e. not using an IAM role or alternative approach).
See [Credentials for the AWS SDK for PHP Version 3](https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials.html) for more information.

```ini
TTRSS_CACHE_S3_ACCESS_KEY=xxx
TTRSS_CACHE_S3_SECRET_KEY=yyy
```
